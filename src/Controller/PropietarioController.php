<?php
namespace App\Controller;

use App\Entity\Propietario;
use App\Form\PropietarioFormType;
use App\Repository\PropietarioRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PropietarioController extends AbstractController
{
    /**
     * @Route("/propietario/list")
     */
    public function list(PropietarioRepository $repository, Request $request, PaginatorInterface $paginator)
    {

        $q = $request->query->get('q');

        $queryBuilder = $repository->getWithSearchQueryBuilder($q);
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('propietario/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }
    /**
     * @Route("/propietario/new", name="app_propietario_new")
     * @IsGranted("ROLE_ADMIN_PROPIETARIO")
     */
    public function new( EntityManagerInterface $em, Request $request)
    {
        $form= $this->createForm(PropietarioFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            dd($form->getData());
        }
        return $this->render('propietario/new.html.twig', [
            'propietarioForm'=> $form->createView()
        ]);;

    }

    /**
     * @Route("/propietario/show/{slug}", name="propietario_show")
     */
    public function show(Propietario $propietario, EntityManagerInterface $em){
        if(!$propietario){
            throw $this->createNotFoundException(sprintf("No existe el propietario %s que quiere mostrar", $cuit));
        }
        return $this->render('propietario/show.html.twig', [
            'propietario' => $propietario
        ]);
    }

    /**
     * @Route("/propietario/{id}/edit")
     * @IsGranted("MANAGE", subject="propietario")
     */
    public function edit( Propietario $propietario ){
        //$this->denyAccessUnlessGranted('MANAGE', $propietario);
        dd($propietario);
    }

}