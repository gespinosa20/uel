<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PropietarioFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
          ->add('renspa')
          ->add('razonSocial')
          ->add('domicilio')
          ->add('codigoPostal')
          ->add('telefono')
          ->add('cuit');
    }

}